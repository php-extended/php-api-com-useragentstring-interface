# php-extended/php-api-com-useragentstring-interface

A library that gets data from the useragentstring.com website.

![coverage](https://gitlab.com/php-extended/php-api-com-useragentstring-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-api-com-useragentstring-interface ^8`


## Basic Usage

This library is an interface-only library.

For a concrete implementation, see [`php-extended/php-api-com-useragentstring-object`](https://gitlab.com/php-extended/php-api-com-useragentstring-object).


## License

MIT (See [license file](LICENSE)).
