<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-useragentstring-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUseragentstring;

use InvalidArgumentException;
use Iterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;
use Stringable;

/**
 * ApiComUseragentstringEndpointInterface class file.
 * 
 * This class acts as an endpoint for the useragentstring.com API.
 * 
 * @author Anastaszor
 */
interface ApiComUseragentstringEndpointInterface extends Stringable
{
	
	/**
	 * Gets the parsed user agent data from the given string.
	 * 
	 * @param string $userAgentString
	 * @return ApiComUseragentstringUserAgentInterface
	 * @throws ClientExceptionInterface
	 * @throws UnprovidableThrowable
	 * @throws ReificationThrowable
	 * @throws InvalidArgumentException
	 */
	public function getUserAgent(string $userAgentString) : ApiComUseragentstringUserAgentInterface;
	
	/**
	 * Gets the iterator over user agent infos.
	 *
	 * @return Iterator<ApiComUseragentstringInfoInterface>
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 */
	public function getUserAgentInfoIterator() : Iterator;
	
	/**
	 * Gets the data that corresponds to the given user agent info.
	 *
	 * @param ApiComUseragentstringInfoInterface $info
	 * @return ApiComUseragentstringDataInterface
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 */
	public function getUserAgentData(ApiComUseragentstringInfoInterface $info) : ApiComUseragentstringDataInterface;
}
