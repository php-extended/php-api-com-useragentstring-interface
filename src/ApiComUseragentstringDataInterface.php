<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-useragentstring-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUseragentstring;

use DateTimeInterface;
use Stringable;

/**
 * ApiComUseragentstringDataInterface interface file.
 * 
 * This is a data class for extracted informations from the website.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiComUseragentstringDataInterface extends Stringable
{
	
	/**
	 * Gets the id of the user agent string.
	 * 
	 * @return int
	 */
	public function getIdentifier() : int;
	
	/**
	 * Gets the user agent string value.
	 * 
	 * @return string
	 */
	public function getUserAgent() : string;
	
	/**
	 * Gets the parts of the user agent string explained individually.
	 * 
	 * @return array<string, string>
	 */
	public function getParts() : array;
	
	/**
	 * Gets the global description of the user agent.
	 * 
	 * @return ?string
	 */
	public function getDescription() : ?string;
	
	/**
	 * Gets the date when this user agent was seen for the first time.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getFirstVisit() : ?DateTimeInterface;
	
	/**
	 * Gets the date when this user agent was seen for the last time.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getLastVisit() : ?DateTimeInterface;
	
}
