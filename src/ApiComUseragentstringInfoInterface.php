<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-useragentstring-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUseragentstring;

use Stringable;

/**
 * ApiComUseragentstringInfoInterface interface file.
 * 
 * This represents the face info that are available when searching for an user
 * agent value.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiComUseragentstringInfoInterface extends Stringable
{
	
	/**
	 * Gets the family of the user agent.
	 * 
	 * @return string
	 */
	public function getFamily() : string;
	
	/**
	 * Gets the browser of the user agent.
	 * 
	 * @return string
	 */
	public function getBrowser() : string;
	
	/**
	 * Gets the user agent value.
	 * 
	 * @return string
	 */
	public function getUserAgent() : string;
	
	/**
	 * Gets the id of the user agent in the useragentstring database.
	 * 
	 * @return int
	 */
	public function getIdentifier() : int;
	
}
