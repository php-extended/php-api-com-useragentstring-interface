<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-useragentstring-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUseragentstring;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiComUseragentstringUserAgentInterface interface file.
 * 
 * This gathers all the possible data over an user agent.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiComUseragentstringUserAgentInterface extends Stringable
{
	
	/**
	 * Gets the user agent type.
	 * 
	 * @return ?string
	 */
	public function getAgentType() : ?string;
	
	/**
	 * Gets the user agent name.
	 * 
	 * @return ?string
	 */
	public function getAgentName() : ?string;
	
	/**
	 * Gets the user agent version number.
	 * 
	 * @return ?string
	 */
	public function getAgentVersion() : ?string;
	
	/**
	 * Gets the operating system type.
	 * 
	 * @return ?string
	 */
	public function getOsType() : ?string;
	
	/**
	 * Gets the operating system name.
	 * 
	 * @return ?string
	 */
	public function getOsName() : ?string;
	
	/**
	 * Gets the operating system version name.
	 * 
	 * @return ?string
	 */
	public function getOsVersionName() : ?string;
	
	/**
	 * Gets the operating system version number.
	 * 
	 * @return ?string
	 */
	public function getOsVersionNumber() : ?string;
	
	/**
	 * Gets the operating system producer name.
	 * 
	 * @return ?string
	 */
	public function getOsProducer() : ?string;
	
	/**
	 * Gets the operating system producer url.
	 * 
	 * @return ?UriInterface
	 */
	public function getOsProducerUrl() : ?UriInterface;
	
	/**
	 * Gets the linux distribution.
	 * 
	 * @return ?string
	 */
	public function getLinuxDistribution() : ?string;
	
	/**
	 * Gets the user agent language.
	 * 
	 * @return ?string
	 */
	public function getAgentLanguage() : ?string;
	
	/**
	 * Gets the user agent language tag.
	 * 
	 * @return ?string
	 */
	public function getAgentLanguageTag() : ?string;
	
}
